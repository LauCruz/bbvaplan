package com.bbva.plan;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Logger;

public class Clase {
	private static final Logger LOGGER = Logger.getLogger("Calendario");

	public static void main(String args[]) throws ParseException {

		Clase ejm = new Clase();

		Scanner scanner = new Scanner(System.in);

		System.out.print("Ingrese fecha inicio (dd/MM/yyyy):");
		String fechaString = scanner.nextLine();

		System.out.print("Ingrese fecha final (dd/MM/yyyy):");
		String fechaString2 = scanner.nextLine();

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date fecha = sdf.parse(fechaString);
		Date fecha2 = sdf.parse(fechaString2);

		Calendar calendario = Calendar.getInstance();
		calendario.setTime(fecha);

		Calendar calendario2 = Calendar.getInstance();
		calendario2.setTime(fecha2);

		List<String> dates = new ArrayList<String>();
		boolean prove = false;
		while (!calendario2.before(calendario)) {
			if (calendario.get(Calendar.DATE) == 16 || calendario.get(Calendar.DATE) == 1) {

				dates.add(sdf.format(calendario.getTime()));
				prove = true;
			}
			calendario.add(Calendar.DATE, 1);
		}

		 for(String c : dates){
			System.out.println("Fecha: " + c);
		}

	}
}
