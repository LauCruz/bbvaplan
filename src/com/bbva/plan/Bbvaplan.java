package com.bbva.plan;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.TimeZone;

public class Bbvaplan {

	public static void main(String args[]) throws ParseException {
		Scanner sc = new Scanner(System.in);
		System.out.print("�Para cu�ndo quieres ahorrar? (dd/mm/yyyy): ");
		String fechaMetaFinal = sc.nextLine();
		System.out.println("�Cu�nto quieres ahorrar?");
		Double cuanto = sc.nextDouble();
		System.out.println("�Ingreso nominal?");
		Double gastos = sc.nextDouble();
		
			
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		Date testDate = null;
		Date testDateFin = null;

		String date = dateFormat.format(new Date());

		String date1 = date;
		String date2 = fechaMetaFinal;
		try {
			testDate = dateFormat.parse(date1);
			testDateFin = dateFormat.parse(date2);
		} catch (Exception ex) {
			System.out.print("Error al convertir fecha");
		}

		Date fecIn = dateFormat.parse(date1);
		Date fecFi = dateFormat.parse(date2);
		int dias = (int) ((fecFi.getTime() - fecIn.getTime()) / 86400000);
		System.out.println("Dias: "+ dias);

		Calendar calendario = Calendar.getInstance();
		calendario.setTime(fecIn);

		Calendar calendario2 = Calendar.getInstance();
		calendario2.setTime(fecFi);

		//almacenar los d�as 1 y 16 de cada mes
		int con = 0;
		List<String> dates = new ArrayList<String>();
		boolean prove = false;
		while (!calendario2.before(calendario)) {
			if (calendario.get(Calendar.DATE) == 16 || calendario.get(Calendar.DATE) == 1) {
				dates.add(dateFormat.format(calendario.getTime()));
				prove = true;
				con++;
			}
			calendario.add(Calendar.DATE, 1);
		}

		//si hay d�as 16 y 1 proponer programado a usuario
		if (prove == true) {
			//meses:
			double meses = dias / 30;
			meses = Math.round(meses * 100.0) / 100.0;
			System.out.println("Meses: " + meses);
			
			

			//Calculo mensual
			Double aMensual = cuanto / meses;
			Double porcentaje = (aMensual * 100) / gastos;
			
			
			//Calculo quincenal
			Double aQuincenal = cuanto / con;
			Double qPorcentaje = (con *100)  / gastos;
			

			if (porcentaje > 20) {
				Double montoMensual = gastos * .20;
				System.out.println("\n\nTu regla de ahorro es: ");
				System.out.println("Porcentaje: 20%");
				System.out.println("Monto mensual ahorrado por compras con 20%: " + montoMensual );
				Double montoProgramado = gastos * (porcentaje - 20) / 100;
				System.out.println("Monto programado mensual: " + Math.round(montoProgramado));
				
				
				 
				//Double mQuincenal = montoProgramado /2; 
				Double mQuincenal = (montoProgramado / con);
				System.out.println("Monto programado quincenal: " + Math.round(mQuincenal)+ 
						" el cual lograr�s en " + con + " quincenas.");
				
			} else {
				if (porcentaje > 0.0 && porcentaje <= 5.0) {
					porcentaje = 5.0;
				}
				if (porcentaje >= 6. && porcentaje <= 7.10) {
					porcentaje = 10.0;
				}
				if (porcentaje >= 11.10 && porcentaje <= 15.99) {
					porcentaje = 15.0;
				}
				if (porcentaje > 16 && porcentaje <= 19.99) {
					porcentaje = 20.0;
				}
				double montoMensual = gastos * (porcentaje / 100);
				System.out.println("\n\nTu regla de ahorro es: ");
				System.out.println("Porcentaje: " + porcentaje);
				System.out.println("Monto mensual ahorrado por compras con " + porcentaje + " : " + montoMensual);
			}

		}

		if (dias == 0.0 || dias <= 1.0) {
			System.out.print("Te sugerimos realizar un ahorro voluntario: " + cuanto);
		}
		
		

	}
		
		
		
		

}

